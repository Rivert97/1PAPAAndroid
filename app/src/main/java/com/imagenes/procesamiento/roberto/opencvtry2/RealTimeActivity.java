package com.imagenes.procesamiento.roberto.opencvtry2;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.ImageFormat;
import android.hardware.Camera;
import android.net.Uri;
import android.os.Debug;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.content.Intent;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.BaseLoaderCallback;
import org.opencv.android.CameraBridgeViewBase;
import org.opencv.android.JavaCameraView;
import org.opencv.android.LoaderCallbackInterface;
import org.opencv.android.OpenCVLoader;
import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.util.List;
import java.util.UUID;

/***
 * Actividad que hace el color transfer en tiempo real.
 */
public class RealTimeActivity extends AppCompatActivity implements CameraBridgeViewBase.CvCameraViewListener2,
        View.OnClickListener {

    //------------Variables estáticas------------------
    private static String TAG = "RealTimeActivity";
    final static int REQUEST_PERMISSION = 200;
    static int GET_TARGET_IMG = 102;

    //------------Variables para GUI-------------------
    //JavaCameraView javaCameraView;
    MyCameraView javaCameraView;
    Button Target;
    Button ColorSpace;
    Button Save;
    Button btHdr;
    Button btDebug;
    ImageView HDRView;
    //EditText etDebug;

    //------------Variables para Color Transfer--------
    Mat mRgba, mSum,save,zeros;
    Transfer transfer=null;
    boolean Image=false;
    String targetPath;
    int contador=0;
    private final int max = 1;
    final static String folderName = "ColorTransfer";
    String colorSpaces[] = {"RGB", "LAB", "LUV", "HSV", "YCRCB","RLAB"};
    int currentIndex;
    Transfer.Code currentSpace;

    //-------------Carga de librería nativa------------
    static{
        System.loadLibrary("MyOpencvLibs");
    }

    //-------------Callback cuando se carga OpenCV-----
    BaseLoaderCallback mLoaderCallback = new BaseLoaderCallback(this) {
        @Override
        public void onManagerConnected(int status) {
            switch (status)
            {
                case BaseLoaderCallback.SUCCESS:
                    //javaCameraView.setMaxFrameSize(320,280); //Esto si sirve
                    javaCameraView.enableView();
                    break;
                default:
                    super.onManagerConnected(status);
                    break;
            }
        }
    };

    /***
     * Se llama al crear la actividad.
     * @param savedInstanceState Estado previo, si es que existe.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_real_time);

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);

        //---------Check permissions--------------------
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(this, "You must grant permissions to this app", Toast.LENGTH_SHORT).show();
            finish();
        }

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);//Forzar a que sea landscape

        //---------Asociar objetos con GUI del xml------
        javaCameraView = findViewById(R.id.java_camera_view);
        javaCameraView.setVisibility(View.VISIBLE);
        javaCameraView.setCvCameraViewListener(this);
        Target = findViewById(R.id.btGetTarget);
        Target.setOnClickListener(this);
        btDebug = findViewById(R.id.btDebug);
        btDebug.setOnClickListener(this);
        //etDebug = findViewById(R.id.etDebug);
        ColorSpace = findViewById(R.id.btSetSpace);
        ColorSpace.setOnClickListener(this);
        Save = findViewById(R.id.btSaveFrame);
        Save.setOnClickListener(this);
        btHdr = findViewById(R.id.btHDR);
        btHdr.setOnClickListener(this);
        HDRView = findViewById(R.id.HDRView);

        //---------Inicialización del transfer----------
        OpenCVLoader.initDebug();
        transfer = new Transfer();
        currentIndex = 0;
        changeColorSpace();//Will start in LAB

    }

    /***
     * Cambia entre espacios de color en orden RGB, LAB, LUB, HSV, YCRCB
     */
    private void changeColorSpace() {

        if(currentIndex > 5)
            currentIndex = 0;

        switch (currentIndex){
            case 0://RGB
                currentSpace = Transfer.Code.RGB;
                break;
            case 1://LAB
                currentSpace = Transfer.Code.LAB;
                break;
            case 2://LUV
                currentSpace = Transfer.Code.LUV;
                break;
            case 3://HSV
                currentSpace = Transfer.Code.HSV;
                break;
            case 4://YCRCB
                currentSpace = Transfer.Code.YCRCB;
                break;
            case 5://RLAB
                currentSpace = Transfer.Code.RLAB;
                break;
            default:
                return;
        }

        transfer.setColorSpace(currentSpace);
        if(targetPath != null) {
            if(transfer.TargetRGB == null)
                transfer.setTarget(targetPath);
            else
                transfer.updateTarget();
        }
        ColorSpace.setText(colorSpaces[currentIndex%6]);
        currentIndex++;
    }

    /***
     * Callback de clicks de botones.
     * @param v Botón o view al que se dio click.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
        case R.id.btGetTarget://Obtener y cargar el target
            openGallery(GET_TARGET_IMG);
            break;

        case R.id.btSetSpace://Cambiar entre espacios de color
            changeColorSpace();
            break;

        case R.id.btSaveFrame://Guardar imagen actual
            //saveImage();//Guarda el preview.
            javaCameraView.captureImage();//Toma una nueva imagen y la guarda (mayor resolución).
            break;

        case R.id.btHDR://Activar o desactivar el HDR
            javaCameraView.HDRMode(!javaCameraView.isHDR());
            String s = "HDR";
            if(!javaCameraView.isHDR())
                s = "No "+s;
            btHdr.setText(s);
            javaCameraView.HDRLoop();
            break;

        case R.id.btDebug:
            javaCameraView.NightMode(!javaCameraView.isNight());
            String s1 = "Night";
            if(!javaCameraView.isNight())
                s1 = "No "+ s1;
            btDebug.setText(s1);
            break;

        default:
        }
    }

    /***
     * Guarda la imagen ResultRGB (el preview) en el dispositivo dentro de la carpeta Pictures.
     */
    private void saveImage() {
        if(transfer.ResultRGB.empty()) return;

        Mat toSave = new Mat();
        Imgproc.cvtColor(transfer.ResultRGB, toSave, Imgproc.COLOR_RGB2BGR);

        if(isExternalStorageWritable())
        {
            File picsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File newFile = new File(picsDir, UUID.randomUUID() + ".jpg");
            String imageName = newFile.getAbsolutePath();
            Imgcodecs.imwrite(imageName, toSave);
            addImageToGallery(imageName, this);
            Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
            return;
        }

        Toast.makeText(this, "Image not saved", Toast.LENGTH_SHORT).show();
    }

    /***
     * Llama actividad para abrir galería y seleccionar la imagen deseada.
     * @param result Constante que indica el código de la petición. Se utiliza en OnActivityResult.
     */
    public void openGallery(int result)
    {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, result);
    }

    /***
     * Se llama al regresar de una llamada StartActivityForResult.
     * @param requestCode Código de la solicitud.
     * @param resultCode Código de resultado.
     * @param data Información que retorna. En este caso el Uri de la imagen.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null){
            if (requestCode == GET_TARGET_IMG){
                Uri imageUri = data.getData();
                targetPath = getPath(imageUri);

                transfer.setTarget(targetPath);
                Image = true;
                //Load target image
                //targetImg = loadImage(path);
                Toast.makeText(this, "Target image loaded", Toast.LENGTH_SHORT).show();

            }
        }
    }

    /***
     * Se llama al pausar la actividad.
     */
    @Override
    protected void onPause() {
        super.onPause();
        if(javaCameraView != null)
            javaCameraView.disableView();

    }

    /***
     * Se llama al destruir la actividad.
     */
    protected void onDestroy(){
        super.onDestroy();
        if(javaCameraView != null)
            javaCameraView.disableView();
    }

    /***
     * Se llama al resumir la actividad. También la primera vez que se muestra.
     */
    protected void onResume(){
        super.onResume();
        if(OpenCVLoader.initDebug()) {
            Log.i(TAG, "OpenCV loaded successfully");
            mLoaderCallback.onManagerConnected(LoaderCallbackInterface.SUCCESS);
        }
        else{
            Log.i(TAG, "OpenCV not loaded");
            OpenCVLoader.initAsync(OpenCVLoader.OPENCV_VERSION, this, mLoaderCallback);
        }
    }

    /***
     * Se llama cuando el javacameraview comienza a funcionar.
     * @param width -  the width of the frames that will be delivered
     * @param height - the height of the frames that will be delivered
     */
    @Override
    public void onCameraViewStarted(int width, int height) {
        //--------------Variables para hacer transfer----------------
        mRgba = new Mat(height, width, CvType.CV_8UC4);
        mSum = new Mat(height,width,CvType.CV_8UC4);
        save = Mat.zeros(height,width,CvType.CV_8UC4);
        zeros = Mat.zeros(height,width,CvType.CV_8UC4);

        //--------------Configuración de la cámara (idealmente del preview)
        //Log.d("CAM", "ViewStarted");
        //List<int[]> sizes = javaCameraView.getFPSRanges();
        //for(int[] size : sizes)
        //    etDebug.append(Integer.toString(size[0]) + "-" + Integer.toString(size[1]) + "\n");
        //etDebug.append("FINISH\n");
        //javaCameraView.exposureCompensation(12);
        //javaCameraView.setPreviewFPS();
        //javaCameraView.HDRMode(false);
        javaCameraView.setTransfer(transfer);
        javaCameraView.setHDRView(HDRView);
        //javaCameraView.turnOnTheFlash();
        javaCameraView.setMaxPictureSize();
        //Log.d("BEFORE", javaCameraView.getFocusMode());
        /*List<Camera.Size> modes = javaCameraView.();
        for(Camera.Size s : modes)
        {
            Log.d("BEFORE TAKING PICTURE", Integer.toString(s.width) + " " + Integer.toString(s.height));
        }*/
        //javaCameraView.NightMode(false);
    }

    /***
     * Se llama cuando el javacameraview se detiene
     */
    @Override
    public void onCameraViewStopped() {
        mRgba.release();
        mSum.release();
        save.release();
        zeros.release();
    }

    /***
     * Se llama cada que la cámara captura un cuadro.
     * @param inputFrame Cuadro capturado.
     * @return Mat que se va a mostrar.
     */
    @Override
    public Mat onCameraFrame(CameraBridgeViewBase.CvCameraViewFrame inputFrame) {
        mRgba = inputFrame.rgba();
        if (!Image) return mRgba;

       /* if(contador < max)
        {
            Core.add(mSum,mRgba,save,new Mat(),CvType.CV_32F);
            save.copyTo(mSum);
            contador++;
            if(transfer.ResultRGB.empty())
                return mRgba;
            else
                return transfer.ResultRGB;
        }
        else {*/
            contador = 0;
            //Core.divide(mSum,new Scalar(max,max,max),save,1,CvType.CV_8UC3);
            //zeros.copyTo(mSum);
            try {
                transfer.setSource(mRgba);
                transfer.applyTransfer();
            } catch (Exception E) {
                return mRgba;
            }
        //}
        //Log.d("REALTIME", Integer.toString(transfer.ResultRGB.rows()) + " " + transfer.ResultRGB.cols());
        return transfer.ResultRGB;
    }

    /***
     * Obtiene la ruta de la imagen que se retorna al abrir la galería.
     * @param imageUri Imagen retornada por actividad de galería.
     * @return Ruta a la imagen.
     */
    private String getPath(Uri imageUri) {
        if(imageUri == null)
            return null;
        else
        {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(imageUri, projection, null, null, null);

            if(cursor != null)
            {
                int col_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                return cursor.getString(col_index);
            }
            cursor.close();
        }

        return imageUri.getPath();
    }

    /***
     * Verifica si el almacenamiento externo está disponible para escribir.
     * @return True si se puede escribir.
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /***
     * Permite que la imagen sea accesada desde la galería añadiéndola al Content Media (o algo asi).
     * @param filePath Ruta de la imagen a agregar.
     * @param context Contexto actual.
     */
    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
}
