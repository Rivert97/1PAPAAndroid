package com.imagenes.procesamiento.roberto.opencvtry2;

import android.content.ContentValues;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.SurfaceTexture;
import android.hardware.Camera;
import android.hardware.Camera.PictureCallback;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import org.opencv.android.JavaCameraView;
import org.opencv.android.Utils;
import org.opencv.core.Mat;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.UUID;

public class MyCameraView extends JavaCameraView {
    private boolean isHdr=false;
    private boolean isNight = false;
    private Transfer transfer;
    private ImageView HDRView;

    public MyCameraView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setHDRView(ImageView v)
    {
        HDRView = v;
    }

    /***
     * Modifica el rango de FPS del preview. La lista contiene los rangos válidos y el índice más grande es el mayor rango.
     */
    public void setPreviewFPS() {//Actualmente no funciona
        mCamera.stopPreview();
        List<int[]> fpsRange;
        Camera.Parameters params = mCamera.getParameters();
        fpsRange = params.getSupportedPreviewFpsRange();
        params.setPreviewFpsRange(fpsRange.get(0)[0], fpsRange.get(0)[1]);
        mCamera.setParameters(params);
        mCamera.startPreview();
    }

    /***
     * Modifica el tiempo de exposición antes de tomar un frame. No todos los dispositivos lo soportan.
     * @param exposure Tiempo de exposición
     */
    public void exposureCompensation(int exposure) {//Actualmente no funciona
        mCamera.stopPreview();
        Camera.Parameters params = mCamera.getParameters();
        params.setExposureCompensation(exposure);
        params.setAutoExposureLock(true);
        mCamera.setParameters(params);
        //Toast.makeText(getContext(), Integer.toString(params.getMinExposureCompensation()), Toast.LENGTH_LONG).show();
        //Toast.makeText(getContext(), Integer.toString(params.getMaxExposureCompensation()), Toast.LENGTH_LONG).show();
        mCamera.startPreview();
    }

    /***
     * Guarda la referencia al objeto transfer para ciertas operaciones.
     * @param transfer1 Objeto de la clase Transfer.
     */
    void setTransfer(Transfer transfer1)
    {
        transfer = transfer1;
        //mCamera.stopPreview();
    }

    /***
     *
     * @return Verdadero si el modo HDR está activado.
     */
    boolean isHDR()
    {
        return isHdr;
    }
    public void HDRMode(boolean IsHDR) {
        mCamera.stopPreview();
        isHdr = IsHDR;
        if(IsHDR) {
            Camera.Parameters params = mCamera.getParameters();
            params.setSceneMode(Camera.Parameters.SCENE_MODE_HDR);
            mCamera.setParameters(params);
        }else {
            Camera.Parameters params = mCamera.getParameters();
            params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
            mCamera.setParameters(params);
        }
        mCamera.startPreview();
    }

    /***
     * Enciende el flash en modo antorcha (siempre encendido).
     */
    public void turnOnTheFlash() {
        mCamera.stopPreview();
        Camera.Parameters params = mCamera.getParameters();
        params.setFlashMode(Camera.Parameters.FLASH_MODE_TORCH);
        mCamera.setParameters(params);
        mCamera.startPreview();
    }


    /***
     * Apaga el flash.
     */
    public void turnOffTheFlash() {
        mCamera.stopPreview();
        Camera.Parameters params = mCamera.getParameters();
        params.setFlashMode(Camera.Parameters.FLASH_MODE_OFF);
        mCamera.setParameters(params);
        mCamera.startPreview();
    }

    /**
     *
     * @return Lista de tamaños posibles de preview.
     */
    public Camera.Size getPreviewSize() {
        Camera.Parameters params = mCamera.getParameters();
        return params.getPreviewSize();
    }

    /***
     *
     * @return Lista de tamaños posibles para imagen (fotografía).
     */
    public List<Camera.Size> getPictureSizes() {
        Camera.Parameters params = mCamera.getParameters();
        return params.getSupportedPictureSizes();
    }

    /***
     *
     * @return Lista de rangos de FPS para el preview.
     */
    public List<int[]> getFPSRanges(){
        return mCamera.getParameters().getSupportedPreviewFpsRange();
    }

    /***
     * Establece el picture size al máximo
     */
    public void setMaxPictureSize() {
        mCamera.stopPreview();
        //disconnectCamera(); //En un lado encontré que habia que conectar y desconectar
        Camera.Parameters params = mCamera.getParameters();
        List<Camera.Size> s = params.getSupportedPictureSizes();
        params.setPictureSize(s.get(2).width, s.get(2).height);
        mCamera.setParameters(params);
        //connectCamera(getWidth(), getHeight());
        mCamera.startPreview();
    }

    public String getFocusMode()
    {
        return mCamera.getParameters().getFocusMode();
    }

    /***
     * Toma una nueva imagen y la guarda en formato jpeg con la resolución establecida en pictureSize.
     */
    public void captureImage()
    {
        mCamera.takePicture(null, null, new PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                mCamera.startPreview();
                mCamera.setPreviewCallback(MyCameraView.this);

                if(isExternalStorageWritable())
                {
                    File picsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
                    File newFile = new File(picsDir, UUID.randomUUID() + ".jpg");
                    String imageName = newFile.getAbsolutePath();
                    //Imgcodecs.imwrite(imageName, toSave);
                    Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
                    Mat src= new Mat();
                    Utils.bitmapToMat(picture,src);

                    transfer.setSource(src);
                    transfer.applyTransfer();
                    if(transfer.ResultRGB.empty())
                        Utils.matToBitmap(src,picture);
                    else {
                        transfer.setFullAlpha();
                        Utils.matToBitmap(transfer.ResultRGB, picture);
                    }

                    try {
                        FileOutputStream fos = new FileOutputStream(newFile);
                        picture.compress(Bitmap.CompressFormat.JPEG, 90, fos);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    addImageToGallery(imageName, getContext());
                    Toast.makeText(getContext(), "Saved", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    public void HDRLoop()
    {
        HDRView.setVisibility(VISIBLE);
        mCamera.takePicture(null, null, new PictureCallback() {
            @Override
            public void onPictureTaken(byte[] data, Camera camera) {
                mCamera.startPreview();
                mCamera.setPreviewCallback(MyCameraView.this);

                    //Imgcodecs.imwrite(imageName, toSave);
                    Bitmap picture = BitmapFactory.decodeByteArray(data, 0, data.length);
                    Mat src= new Mat();
                    Utils.bitmapToMat(picture,src);

                    transfer.setSource(src);
                    transfer.applyTransfer();
                    if(transfer.ResultRGB.empty())
                        Utils.matToBitmap(src,picture);
                    else {
                        transfer.setFullAlpha();
                        Utils.matToBitmap(transfer.ResultRGB, picture);
                    }

                    //Intento raro de HDR
                    HDRView.setImageBitmap(picture);
                    if(isHdr)
                        HDRLoop();
                    else
                        HDRView.setVisibility(GONE);
                }
        });
    }


    /***
     * Verifica si el almacenamiento externo está disponible para escribir.
     * @return True si se puede escribir.
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /***
     * Permite que la imagen sea accesada desde la galería añadiéndola al Content Media (o algo asi).
     * @param filePath Ruta de la imagen a agregar.
     * @param context Contexto actual.
     */
    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    public List<String> getCaptureModes()
    {
        return mCamera.getParameters().getSupportedSceneModes();
    }

    public boolean isNight(){return isNight;}
    public void NightMode(boolean IsNight) {
        mCamera.stopPreview();
        isNight = IsNight;
        if(isNight) {
            Camera.Parameters params = mCamera.getParameters();
            params.setSceneMode(Camera.Parameters.SCENE_MODE_NIGHT);
            mCamera.setParameters(params);
        }else {
            Camera.Parameters params = mCamera.getParameters();
            params.setSceneMode(Camera.Parameters.SCENE_MODE_AUTO);
            mCamera.setParameters(params);
        }
        mCamera.startPreview();
    }


}
