package com.imagenes.procesamiento.roberto.opencvtry2;

import android.Manifest;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Point;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Display;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.Space;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.android.Utils;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Size;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

/***
 * Actividad que procesa una sola imagen.
 */
public class SingleImageActivity extends AppCompatActivity implements View.OnClickListener {

    static int GET_SOURCE_IMG = 101;
    static int GET_TARGET_IMG = 102;

    ImageView ivImage;
    Button btSource;
    Button btTarget;
    Button btSave;
    Button btSpace;

    String colorSpaces[] = {"RGB", "LAB", "LUV", "HSV", "YCRCB","RLAB"};
    int currentIndex;
    Transfer.Code currentSpace;

    String targetPath;
    String sourcePath;

    //Mat sourceImg;
    //Mat targetImg;

    Transfer Trans;

    static{
        System.loadLibrary("MyOpencvLibs");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_image);

        //Check permissions
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
        || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            Toast.makeText(this, "You must grant permissions to this app", Toast.LENGTH_SHORT).show();
            finish();
        }

        OpenCVLoader.initDebug();

        ivImage = findViewById(R.id.ivImage);
        btSource = findViewById(R.id.btChooseSource);
        btTarget = findViewById(R.id.btChooseTarget);
        btSave = findViewById(R.id.btSave);
        btSpace = findViewById(R.id.btColor);

        btSource.setOnClickListener(this);
        btTarget.setOnClickListener(this);
        btSave.setOnClickListener(this);
        btSpace.setOnClickListener(this);

        Trans = new Transfer();
        currentIndex = 1;
        changeColorSpace();//Will start in LAB
    }

    /***
     * Cambia entre espacios de color en el orden: RGB, LAB, LUB, HSB, YCrCb
     */
    private void changeColorSpace() {
        if(currentIndex > 5)
            currentIndex = 0;

        switch (currentIndex){
            case 0://RGB
                currentSpace = Transfer.Code.RGB;
                break;
            case 1://LAB
                currentSpace = Transfer.Code.LAB;
                break;
            case 2://LUV
                currentSpace = Transfer.Code.LUV;
                break;
            case 3://HSV
                currentSpace = Transfer.Code.HSV;
                break;
            case 4://YCRCB
                currentSpace = Transfer.Code.YCRCB;
                break;
            case 5:
                currentSpace = Transfer.Code.RLAB;
                break;
            default:
                return;
        }
        Trans.setColorSpace(currentSpace);
        if(targetPath != null) {
            if(Trans.TargetRGB == null || Trans.SourceRGB == null) {
                Trans.setTarget(targetPath);
                Trans.setSource(sourcePath);
            }
            else {
                Trans.updateTarget();
                Trans.updateSourse();
            }
        }
        Trans.applyTransfer();

        if(Trans.SourceRGB != null && Trans.TargetRGB != null)
            displayImage(Trans.ResultRGB);

        btSpace.setText(colorSpaces[currentIndex%6]);
        currentIndex++;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btChooseSource:
                openGallery(GET_SOURCE_IMG);
                break;

            case R.id.btChooseTarget:
                openGallery(GET_TARGET_IMG);
                break;

            case R.id.btColor:
                changeColorSpace();
                break;

            case R.id.btSave:
                saveImage();
                break;

            default:
        }
    }

    /***
     * Llama actividad para abrir galería y seleccionar la imagen deseada.
     * @param result Constante que indica el código de la petición. Se utiliza en OnActivityResult.
     */
    public void openGallery(int result)
    {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, result);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        displayImage(Trans.ResultRGB);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null){
            if(requestCode == GET_SOURCE_IMG) {
                Uri imageUri = data.getData();
                sourcePath = getPath(imageUri);

                //Load source image
                Trans.setSource(sourcePath);
                //sourceImg = loadImage(path);

                //Color transfer
                Trans.applyTransfer();

                if(Trans.SourceRGB != null && Trans.TargetRGB != null) {
                    displayImage(Trans.ResultRGB);
                }
            }
            else if (requestCode == GET_TARGET_IMG){

                Uri imageUri = data.getData();
                targetPath = getPath(imageUri);

                //Load target image
                //targetImg = loadImage(path);
                Trans.setTarget(targetPath);
                Toast.makeText(this, "Target image loaded", Toast.LENGTH_SHORT).show();
                Trans.applyTransfer();

                if(Trans.SourceRGB != null && Trans.TargetRGB != null)
                    displayImage(Trans.ResultRGB);
            }
        }
    }

    /***
     * Guarda la imagen ResultRGB en el dispositivo dentro de la carpeta Pictures.
     */
    private void saveImage() {
        if(Trans.ResultRGB.empty()) return;

        Mat toSave = new Mat();
        Imgproc.cvtColor(Trans.ResultRGB, toSave, Imgproc.COLOR_RGB2BGR);

        if(isExternalStorageWritable())
        {
            File picsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File newFile = new File(picsDir, UUID.randomUUID() + ".jpg");
            String imageName = newFile.getAbsolutePath();
            Imgcodecs.imwrite(imageName, toSave);
            addImageToGallery(imageName, this);
            Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
            return;
        }

        Toast.makeText(this, "Image not saved", Toast.LENGTH_SHORT).show();
    }

    /***
     * Muestra la imagen en el SurfaceView de la actividad.
     * @param mat Mat a mostrar.
     */
    private void displayImage(Mat mat) {
        //Convert from BGR to RGB
        Mat imageToDisplay = new Mat();
        mat.copyTo(imageToDisplay);

        //Resize to fit in screen
        Display display = getWindowManager().getDefaultDisplay();

        Point size = new Point();
        display.getSize(size);
        int mobile_width = size.x;
        int mobile_height = size.y;

        double downSampleRatio = calculateSubSampleSize(imageToDisplay, mobile_width, mobile_height);
        Imgproc.resize(imageToDisplay, imageToDisplay, new Size(), downSampleRatio, downSampleRatio, Imgproc.INTER_LINEAR);

        //Convert Mat to Bitmap and show
        Bitmap bitmap = Bitmap.createBitmap(imageToDisplay.cols(), imageToDisplay.rows(), Bitmap.Config.RGB_565);
        //convert mat to bitmap
        Utils.matToBitmap(imageToDisplay, bitmap);
        ivImage.setImageBitmap(bitmap);
    }

    /***
     * Calcula el factor de reducción para poder mostrar imagen en SurfaceView.
     * @param src Mat que se quiere mostrar.
     * @param mobile_width Ancho de la pantalla.
     * @param mobile_height Alto de la pantalla.
     * @return Factor por el que se debe escalar.
     */
    private double calculateSubSampleSize(Mat src, int mobile_width, int mobile_height) {
        final int width = src.width();
        final int height = src.height();
        double inSampleSize = 1;

        if(height > mobile_height || width > mobile_width)
        {
            final double heightRatio = (double)mobile_height/(double)height;
            final double widthRatio = (double)mobile_width/(double)width;

            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;
        }

        return inSampleSize;
    }

    /***
     * Obtiene la ruta de la imagen que se retorna al abrir la galería.
     * @param imageUri Imagen retornada por actividad de galería.
     * @return Ruta a la imagen.
     */
    private String getPath(Uri imageUri) {
        if(imageUri == null)
            return null;
        else
        {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(imageUri, projection, null, null, null);

            if(cursor != null)
            {
                int col_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                return cursor.getString(col_index);
            }
            cursor.close();
        }
        
        return imageUri.getPath();
    }

    /***
     * Verifica si el almacenamiento externo está disponible para escribir.
     * @return True si se puede escribir.
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /***
     * Permite que la imagen sea accesada desde la galería añadiéndola al Content Media (o algo asi).
     * @param filePath Ruta de la imagen a agregar.
     * @param context Contexto actual.
     */
    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }
}
