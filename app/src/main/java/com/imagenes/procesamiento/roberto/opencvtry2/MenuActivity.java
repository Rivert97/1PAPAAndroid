package com.imagenes.procesamiento.roberto.opencvtry2;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

/***
 * Actividad que muestra el menú.
 */
public class MenuActivity extends AppCompatActivity implements View.OnClickListener,
        ActivityCompat.OnRequestPermissionsResultCallback {

    Button btSingleImage;
    Button btRealTime;
    Button btCamera2;
    final int REQUEST_PERMISSION = 200;

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        btSingleImage = findViewById(R.id.btSingleImage);
        btRealTime = findViewById(R.id.btRealTime);
        btCamera2 = findViewById(R.id.btCamera2);

        btRealTime.setOnClickListener(this);
        btSingleImage.setOnClickListener(this);
        btCamera2.setOnClickListener(this);

        //Ask permissions
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED
                || ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED)
        {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA,
                            Manifest.permission.READ_EXTERNAL_STORAGE},
                    REQUEST_PERMISSION);
        }
    }

    @Override
    public void onClick(View v) {
        Intent i;
        switch (v.getId()){
            case R.id.btRealTime:
                i = new Intent(this, RealTimeActivity.class);
                startActivity(i);
                break;
            case R.id.btCamera2:
                i = new Intent(this, Camera2Activity.class);
                startActivity(i);
                break;

            case R.id.btSingleImage:
                i = new Intent(this, SingleImageActivity.class);
                startActivity(i);
                break;

            default:
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case RealTimeActivity.REQUEST_PERMISSION: {
                for (int grantResult : grantResults) {
                    if (grantResult == PackageManager.PERMISSION_DENIED) {
                        finish();
                    }
                }
            }
        }
    }
}
