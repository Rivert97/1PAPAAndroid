package com.imagenes.procesamiento.roberto.opencvtry2;

import android.Manifest;
import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.ImageFormat;
import android.graphics.SurfaceTexture;
import android.hardware.camera2.CameraAccessException;
import android.hardware.camera2.CameraCaptureSession;
import android.hardware.camera2.CameraCharacteristics;
import android.hardware.camera2.CameraDevice;
import android.hardware.camera2.CameraManager;
import android.hardware.camera2.CaptureRequest;
import android.hardware.camera2.params.StreamConfigurationMap;
import android.media.Image;
import android.media.ImageReader;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.HandlerThread;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import android.util.Size;
import android.util.SparseIntArray;
import android.view.Surface;
import android.view.TextureView;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import org.opencv.android.OpenCVLoader;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

import java.io.File;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class Camera2Activity extends Activity implements View.OnClickListener {

    //-----------Variables estáticas-------------------
    private static final int REQUEST_CAMERA_PERMISSION = 200;
    static int GET_TARGET_IMG = 102;
    private static final int mImageFormat = ImageFormat.YUV_420_888;

    //----------------Variables para GUI---------------
    private TextureView textureView;
    Button Target;
    Button ColorSpace;
    Button Save;
    Button btHdr;

    //----------------Variables para Color Transfer----
    Mat mRgb, mSum, save,zeros;
    Transfer transfer=null;
    boolean Image=false;
    private String targetPath;
    String colorSpaces[] = {"RGB", "LAB", "LUV", "HSV", "YCRCB","RLAB"};
    int currentIndex;
    Transfer.Code currentSpace;

    //-------------Varaibles para Camera2---------------
    private String cameraId;
    private CameraDevice cameraDevice;
    private CameraCaptureSession cameraCaptureSessions;
    private CaptureRequest.Builder captureRequestBuilder;
    private final Object mStateLock = new Object();
    private Size imageDimension;
    private RefCountedAutoCloseable<ImageReader> imageReader;
    private Surface mSurface;
    private Handler mBackgroundHandler;
    private HandlerThread mBackgroundThread;

    //--------Check state orientation of output image---
    private static final SparseIntArray ORIENTATIONS = new SparseIntArray();
    static{
        ORIENTATIONS.append(Surface.ROTATION_0,90);
        ORIENTATIONS.append(Surface.ROTATION_90,0);
        ORIENTATIONS.append(Surface.ROTATION_180,270);
        ORIENTATIONS.append(Surface.ROTATION_270,180);
    }

    //----------------Cargar librería nativa---------------
    static{
        System.loadLibrary("MyOpencvLibs");
    }

    //------------Listener para la textureView-------------
    TextureView.SurfaceTextureListener textureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            openCamera();
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {

        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {

        }
    };

    //------------Callback para el estado de la camara--------------
    CameraDevice.StateCallback stateCallback = new CameraDevice.StateCallback() {
        @Override
        public void onOpened(@NonNull CameraDevice camera) {
            cameraDevice = camera;
            createCameraPreview();
        }

        @Override
        public void onDisconnected(@NonNull CameraDevice cameraDevice) {
            cameraDevice.close();
        }

        @Override
        public void onError(@NonNull CameraDevice cameraDevice, int i) {
            cameraDevice.close();
            cameraDevice=null;
        }
    };


    /***
     * Se llama al crear la actividad.
     * @param savedInstanceState Estado previo, si es que existe.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_camera2);

        //-----Asociar variables con GUI de xml----------
        textureView = findViewById(R.id.textureView);
        //From Java 1.4 , you can use keyword 'assert' to check expression true or false
        assert textureView != null;
        textureView.setSurfaceTextureListener(textureListener);
        Target = findViewById(R.id.btGetTarget2);
        Target.setOnClickListener(this);
        //etDebug = findViewById(R.id.etDebug);
        ColorSpace = findViewById(R.id.btSetSpace2);
        ColorSpace.setOnClickListener(this);
        Save = findViewById(R.id.btSaveFrame2);
        Save.setOnClickListener(this);
        btHdr = findViewById(R.id.btHDR2);
        btHdr.setOnClickListener(this);

        //----------Inicializar el transfer-------------
        OpenCVLoader.initDebug();
        transfer = new Transfer();
        currentIndex = 0;
        changeColorSpace();//Will start in RGB
        //transfer.setColorSpace(Transfer.Code.LAB);
        mRgb = new Mat();
    }

    /***
     * Crea el preview de la cámara. AQUÍ SE CONFIGURA A CUÁLES SUPERFICIES SE DIRIGIRÁ LA IMAGEN CAPTURADA.
     */
    private void createCameraPreview() {
        try{
            SurfaceTexture texture = textureView.getSurfaceTexture();
            assert  texture != null;
            texture.setDefaultBufferSize(imageDimension.getWidth(),imageDimension.getHeight());

            //------------Superficies que reciben la imagen------------
            List<Surface> surfaces = new ArrayList<>();
            mSurface = new Surface(texture);
            //surfaces.add(mSurface);
            surfaces.add(imageReader.get().getSurface());
            captureRequestBuilder = cameraDevice.createCaptureRequest(CameraDevice.TEMPLATE_PREVIEW);
            //captureRequestBuilder.addTarget(surfaces.get(0));
            captureRequestBuilder.addTarget(imageReader.get().getSurface());

            cameraDevice.createCaptureSession(surfaces, new CameraCaptureSession.StateCallback() {
                @Override
                public void onConfigured(@NonNull CameraCaptureSession cameraCaptureSession) {
                    if(cameraDevice == null)
                        return;
                    cameraCaptureSessions = cameraCaptureSession;
                    updatePreview();
                }

                @Override
                public void onConfigureFailed(@NonNull CameraCaptureSession cameraCaptureSession) {
                    Toast.makeText(Camera2Activity.this, "Changed", Toast.LENGTH_SHORT).show();
                }
            },null);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /***
     * AQUÍ SE CONFIGURA EL PREVIEW
     */
    private void updatePreview() {
        if(cameraDevice == null)
            Toast.makeText(this, "Error", Toast.LENGTH_SHORT).show();
        //-----------Configuración del preview--------------
        captureRequestBuilder.set(CaptureRequest.CONTROL_MODE,CaptureRequest.CONTROL_MODE_AUTO);
        //captureRequestBuilder.set(CaptureRequest.FLASH_MODE, CaptureRequest.FLASH_MODE_TORCH);


        try{
            cameraCaptureSessions.setRepeatingRequest(captureRequestBuilder.build(),null, mBackgroundHandler);
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /***
     * AQUÍ SE CONFIGURA LA CÁMARA Y EL IMAGEREADER QUE RECIBE LA IMAGEN.
     */
    private void openCamera() {
        CameraManager manager = (CameraManager)getSystemService(Context.CAMERA_SERVICE);
        try{
            cameraId = manager.getCameraIdList()[0];
            CameraCharacteristics characteristics = manager.getCameraCharacteristics(cameraId);
            StreamConfigurationMap map = characteristics.get(CameraCharacteristics.SCALER_STREAM_CONFIGURATION_MAP);
            assert map != null;
            imageDimension = map.getOutputSizes(SurfaceTexture.class)[0];

            //Check realtime permission if run higher API 23
            if(ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
            {
                ActivityCompat.requestPermissions(this,new String[]{
                        Manifest.permission.CAMERA,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE
                },REQUEST_CAMERA_PERMISSION);
                return;
            }
            manager.openCamera(cameraId,stateCallback,null);

            //-----------Configuración del image reader----------------
            synchronized (mStateLock) {
                if (imageReader == null || imageReader.getAndRetain() == null) {
                    imageReader = new RefCountedAutoCloseable<>(
                            ImageReader.newInstance(imageDimension.getWidth(), imageDimension.getHeight(), mImageFormat, 1));
                }
                imageReader.get().setOnImageAvailableListener(mOnImageAvailableListener, mBackgroundHandler);
            }
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
    }

    /***
     * Listener de cuando hay una imágen disponible en image reader
     */
    private final ImageReader.OnImageAvailableListener mOnImageAvailableListener
            = new ImageReader.OnImageAvailableListener() {

        @Override
        public void onImageAvailable(ImageReader reader) {

            Image image;
            
            try {
                image = reader.acquireLatestImage();
                if( image == null) {
                    return;
                }
                //int fmt = reader.getImageFormat();

                //----------Procesar imagen----------
                //ProcessAsMat(image, mSurface);
                //ProcessImg(image, mSurface);
                GetMat(image, mRgb);
                if(Image)
                {
                    try {
                        transfer.setSource(mRgb);
                        transfer.applyTransfer();
                        ShowMat(transfer.ResultRGB, mSurface);
                    } catch (Exception E) {
                        Log.d("IMGRDR", "Can't apply transfer");
                        ShowMat(mRgb, mSurface);
                    }
                }
                else
                {
                    ShowMat(mRgb, mSurface);
                }
            } catch (IllegalStateException e) {
                Log.e("IMGRDR", "Too many images queued for saving, dropping image for request: ");
                return;
            }
            image.close();

        }
    };

    /***
     * Se llama despues de pedir los permisos
     * @param requestCode Identificador de la request.
     * @param permissions Permisos que se pidieron.
     * @param grantResults Respuesta a cada permiso.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == REQUEST_CAMERA_PERMISSION)
        {
            if(grantResults[0] != PackageManager.PERMISSION_GRANTED)
            {
                Toast.makeText(this, "You can't use camera without permission", Toast.LENGTH_SHORT).show();
                finish();
            }
        }
    }

    /***
     * Se llama al resumir la actividad. También la primera vez que se muestra.
     */
    @Override
    protected void onResume() {
        super.onResume();
        startBackgroundThread();
        if(textureView.isAvailable()) {
            openCamera();
            //Reanudar repeating request de la camara
            updatePreview();
        }
        else
            textureView.setSurfaceTextureListener(textureListener);
    }

    /***
     * Se llama al pausar la actividad.
     */
    @Override
    protected void onPause() {
        //Stop repeating request de la camara
        try {
            cameraCaptureSessions.stopRepeating();
        } catch (CameraAccessException e) {
            e.printStackTrace();
        }
        stopBackgroundThread();
        super.onPause();
    }

    /***
     * Detiene el Hilo que corre en el background
     */
    private void stopBackgroundThread() {
        mBackgroundThread.quitSafely();
        try{
            mBackgroundThread.join();
            mBackgroundThread= null;
            synchronized (mStateLock) {
                mBackgroundHandler = null;
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /***
     * Inicia el hilo que corre en el background.
     */
    private void startBackgroundThread() {
        mBackgroundThread = new HandlerThread("Camera Background");
        mBackgroundThread.start();
        synchronized (mStateLock) {
            mBackgroundHandler = new Handler(mBackgroundThread.getLooper());
        }
    }

    /***
     * Callback de clicks de botones.
     * @param v Botón o view al que se dio click.
     */
    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btGetTarget2:
                openGallery(GET_TARGET_IMG);
                break;

            case R.id.btSetSpace2:
                changeColorSpace();
                break;

            case R.id.btSaveFrame2:
                saveImage();
                break;
            case R.id.btHDR2:
                /*javaCameraView.HDRMode(!javaCameraView.isHDR());
                String s = "HDR";
                if(!javaCameraView.isHDR())
                    s = "No "+s;
                btHdr.setText(s);*/
                break;
            default:
        }
    }

    /***
     * Clase que almacena los imagereader para que funcionen correctamente con el hilo de la camara.
     * @param <T>
     */
    public static class RefCountedAutoCloseable<T extends AutoCloseable> implements AutoCloseable {
        private T mObject;
        private long mRefCount = 0;

        /**
         * Wrap the given object.
         *
         * @param object an object to wrap.
         */
        public RefCountedAutoCloseable(T object) {
            if (object == null) throw new NullPointerException();
            mObject = object;
        }

        public synchronized T getAndRetain() {
            if (mRefCount < 0) {
                return null;
            }
            mRefCount++;
            return mObject;
        }

        /**
         * Return the wrapped object.
         *
         * @return the wrapped object, or null if the object has been released.
         */
        public synchronized T get() {
            return mObject;
        }

        /**
         * Decrement the reference count and release the wrapped object if there are no other
         * users retaining this object.
         */
        @Override
        public synchronized void close() {
            if (mRefCount >= 0) {
                mRefCount--;
                if (mRefCount < 0) {
                    try {
                        mObject.close();
                    } catch (Exception e) {
                        throw new RuntimeException(e);
                    } finally {
                        mObject = null;
                    }
                }
            }
        }
    }

    /***
     * Llama actividad para abrir galería y seleccionar la imagen deseada.
     * @param result Constante que indica el código de la petición. Se utiliza en OnActivityResult.
     */
    public void openGallery(int result)
    {
        Intent intent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(intent, result);
    }

    /***
     * Se llama al regresar de una llamada StartActivityForResult.
     * @param requestCode Código de la solicitud.
     * @param resultCode Código de resultado.
     * @param data Información que retorna. En este caso el Uri de la imagen.
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK && data != null){
            if (requestCode == GET_TARGET_IMG){
                Uri imageUri = data.getData();
                targetPath = getPath(imageUri);

                transfer.setTarget(targetPath);
                Image = true;
                //Load target image
                //targetImg = loadImage(path);
                Toast.makeText(this, "Target image loaded", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /***
     * Obtiene la ruta de la imagen que se retorna al abrir la galería.
     * @param imageUri Imagen retornada por actividad de galería.
     * @return Ruta a la imagen.
     */
    private String getPath(Uri imageUri) {
        if(imageUri == null)
            return null;
        else
        {
            String[] projection = {MediaStore.Images.Media.DATA};
            Cursor cursor = getContentResolver().query(imageUri, projection, null, null, null);

            if(cursor != null)
            {
                int col_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
                cursor.moveToFirst();

                return cursor.getString(col_index);
            }
            cursor.close();
        }

        return imageUri.getPath();
    }

    public void GetMat(Image src, Mat dst)
    {
        if (src.getFormat() != ImageFormat.YUV_420_888) {
            throw new IllegalArgumentException("src must have format YUV_420_888.");
        }
        Image.Plane[] planes = src.getPlanes();
        // Spec guarantees that planes[0] is luma and has pixel stride of 1.
        // It also guarantees that planes[1] and planes[2] have the same row and
        // pixel stride.
        if (planes[1].getPixelStride() != 1 && planes[1].getPixelStride() != 2) {
            throw new IllegalArgumentException(
                    "src chroma plane must have a pixel stride of 1 or 2: got "
                            + planes[1].getPixelStride());
        }

        OpenCVNativeClass.GetMat(src.getWidth(), src.getHeight(), planes[0].getBuffer(), dst.getNativeObjAddr());

    }

    public void ShowMat(Mat src, Surface dst)
    {
        OpenCVNativeClass.ShowMat(src.width(), src.height(), src.getNativeObjAddr(), dst);
    }

    /***
     * Procesa la imagen convirtiendola a Mat en el código nativo. El proceso se cambia cambiando la
     * última línea.
     * @param src Imagen a procesar.
     * @param dst Superficie donde se va a mostrar.
     */
    public void ProcessImg(Image src, Surface dst) {

        if (src.getFormat() != ImageFormat.YUV_420_888) {
            throw new IllegalArgumentException("src must have format YUV_420_888.");
        }
        Image.Plane[] planes = src.getPlanes();
        // Spec guarantees that planes[0] is luma and has pixel stride of 1.
        // It also guarantees that planes[1] and planes[2] have the same row and
        // pixel stride.
        if (planes[1].getPixelStride() != 1 && planes[1].getPixelStride() != 2) {
            throw new IllegalArgumentException(
                    "src chroma plane must have a pixel stride of 1 or 2: got "
                            + planes[1].getPixelStride());
        }

        //------------------Procesamiento que se desea aplicar----------------
        OpenCVNativeClass.ProcessWCamera2(src.getWidth(), src.getHeight(), planes[0].getBuffer(), dst);
    }

    /***
     * Procesa la imagen convirtiendola a Mat antes de pasarla a nativo. El proceso se cambia cambiando la
     * última línea.
     * @param src Imagen a procesar.
     * @param dst Superficie donde se va a mostrar.
     */
    public void ProcessAsMat(Image src, Surface dst)
    {
        Mat mat = imageToMat(src);

        //------------- Procsamiento que se desea aplicar-----------------
        OpenCVNativeClass.ProcessMat(src.getWidth(), src.getHeight(), mat.getNativeObjAddr(), dst);
    }

    /***
     * Convierte una Image a Mat. Actualmente no se si funciona bien.
     * @param image Imagen a convertir
     * @return Mat de la imagen.
     */
    public static Mat imageToMat(Image image) {
        ByteBuffer buffer;
        int rowStride;
        int pixelStride;
        int width = image.getWidth();
        int height = image.getHeight();
        int offset = 0;

        Image.Plane[] planes = image.getPlanes();
        byte[] data = new byte[image.getWidth() * image.getHeight() * 3 /2];// ImageFormat.getBitsPerPixel(ImageFormat.YUV_420_888) / 8];
        byte[] rowData = new byte[planes[0].getRowStride()];

        for (int i = 0; i < planes.length; i++) {
            buffer = planes[i].getBuffer();
            rowStride = planes[i].getRowStride();
            pixelStride = planes[i].getPixelStride();
            int w = (i == 0) ? width : width / 2;
            int h = (i == 0) ? height : height / 2;
            for (int row = 0; row < h; row++) {
                int bytesPerPixel = ImageFormat.getBitsPerPixel(ImageFormat.YUV_420_888) / 8;
                if (pixelStride == bytesPerPixel) {
                    int length = w * bytesPerPixel;
                    buffer.get(data, offset, length);

                    // Advance buffer the remainder of the row stride, unless on the last row.
                    // Otherwise, this will throw an IllegalArgumentException because the buffer
                    // doesn't include the last padding.
                    if (h - row != 1) {
                        buffer.position(buffer.position() + rowStride - length);
                    }
                    offset += length;
                } else {

                    // On the last row only read the width of the image minus the pixel stride
                    // plus one. Otherwise, this will throw a BufferUnderflowException because the
                    // buffer doesn't include the last padding.
                    if (h - row == 1) {
                        buffer.get(rowData, 0, width - pixelStride + 1);
                    } else {
                        buffer.get(rowData, 0, rowStride);
                    }

                    for (int col = 0; col < w; col++) {
                        data[offset++] = rowData[col * pixelStride];
                    }
                }
            }
        }

        // Finally, create the Mat.
        Mat mat = new Mat(height + height / 2, width, CvType.CV_8UC1);
        mat.put(0, 0, data);

        return mat;
    }

    /***
     * Guarda la imagen ResultRGB (el preview) en el dispositivo dentro de la carpeta Pictures.
     */
    private void saveImage() {
        if(transfer.ResultRGB.empty()) return;
        //if(mRgb.empty()) return;

        Mat toSave = new Mat();
        Imgproc.cvtColor(transfer.ResultRGB, toSave, Imgproc.COLOR_RGB2BGR);

        if(isExternalStorageWritable())
        {
            File picsDir = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
            File newFile = new File(picsDir, UUID.randomUUID() + ".jpg");
            String imageName = newFile.getAbsolutePath();
            Imgcodecs.imwrite(imageName, toSave);
            addImageToGallery(imageName, this);
            Toast.makeText(this, "Saved", Toast.LENGTH_LONG).show();
            return;
        }

        Toast.makeText(this, "Image not saved", Toast.LENGTH_SHORT).show();
    }

    /***
     * Verifica si el almacenamiento externo está disponible para escribir.
     * @return True si se puede escribir.
     */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /***
     * Permite que la imagen sea accesada desde la galería añadiéndola al Content Media (o algo asi).
     * @param filePath Ruta de la imagen a agregar.
     * @param context Contexto actual.
     */
    public static void addImageToGallery(final String filePath, final Context context) {

        ContentValues values = new ContentValues();

        values.put(MediaStore.Images.Media.DATE_TAKEN, System.currentTimeMillis());
        values.put(MediaStore.Images.Media.MIME_TYPE, "image/jpeg");
        values.put(MediaStore.MediaColumns.DATA, filePath);

        context.getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values);
    }

    /***
     * Cambia entre espacios de color en orden RGB, LAB, LUB, HSV, YCRCB
     */
    private void changeColorSpace() {

        if(currentIndex > 5)
            currentIndex = 0;

        switch (currentIndex){
            case 0://RGB
                currentSpace = Transfer.Code.RGB;
                break;
            case 1://LAB
                currentSpace = Transfer.Code.LAB;
                break;
            case 2://LUV
                currentSpace = Transfer.Code.LUV;
                break;
            case 3://HSV
                currentSpace = Transfer.Code.HSV;
                break;
            case 4://YCRCB
                currentSpace = Transfer.Code.YCRCB;
                break;
            case 5://RLAB
                currentSpace = Transfer.Code.RLAB;
                break;
            default:
                return;
        }

        transfer.setColorSpace(currentSpace);
        if(targetPath != null) {
            if(transfer.TargetRGB == null)
                transfer.setTarget(targetPath);
            else
                transfer.updateTarget();
        }
        ColorSpace.setText(colorSpaces[currentIndex%6]);
        currentIndex++;
    }

}
