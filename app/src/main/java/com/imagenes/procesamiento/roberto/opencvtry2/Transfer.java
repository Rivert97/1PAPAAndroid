package com.imagenes.procesamiento.roberto.opencvtry2;

import org.opencv.core.Core;
import org.opencv.core.CvType;
import org.opencv.core.Mat;
import org.opencv.core.MatOfDouble;
import org.opencv.core.Scalar;
import org.opencv.imgcodecs.Imgcodecs;
import org.opencv.imgproc.Imgproc;

/***
 *  Contiene metodos para aplicar Color Transfer a una imagen.
 */
public class Transfer {
    private Mat original;
    private Mat Source;
    public Mat SourceRGB;
    private Mat Target;
    public Mat TargetRGB;
    private Mat Result;
    public Mat ResultRGB;
    public Mat auxS;
    public Mat auxT;

    private MatOfDouble mUs;
    private MatOfDouble mUt;
    private MatOfDouble mDs;
    private MatOfDouble mDt;

    private Scalar Us;
    private Scalar Ut;
    private Scalar Ds;
    private Scalar Dt;

    private boolean isRGB;
    private boolean isRLAB;
    private int convCode;
    private int invConvCode;

    public enum Code {RGB, LAB, LUV, HSV, YCRCB, RLAB}

    /***
     * Crea las intancias de las matrices para que no sean null.
     */
    public Transfer(){
        Source = new Mat();
        Target = new Mat();
        Result = new Mat();
        ResultRGB = new Mat();

        mUs = new MatOfDouble();
        mUt = new MatOfDouble();
        mDs = new MatOfDouble();
        mDt = new MatOfDouble();

        auxS = new Mat();
    }

    /***
     * Carga como source la imagen desde la ruta especificada.
     * @param path Ruta de la imagen a leer.
     * @return True si pudo cargar la imagen.
     */
    public boolean setSource(String path){
        if(path == null) return false;
        if(SourceRGB == null)
            SourceRGB = new Mat();
        Imgproc.cvtColor(Imgcodecs.imread(path), SourceRGB,Imgproc.COLOR_BGR2RGB);
        if(SourceRGB.empty()) return false;
        updateSourse();

        return true;
    }

    /***
     * Asigna como source la Mat indicada. Crea una copia.
     * @param sourse Mat RGB o BGR.
     * @return True si pudo copiar la imagen.
     */
    public boolean setSource(Mat sourse){
        if(SourceRGB == null)
            SourceRGB = new Mat();
        else
            sourse.copyTo(SourceRGB);
        updateSourse();

        return true;
    }

    /***
     * Retorna la Mat donde se almacena el resultado.
     * @return Mat resultado del Color transfer.
     */
    public Mat getResult()
    {
        return ResultRGB;
    }

    /***
     * Carga como target la imagen desde la ruta especificada.
     * @param path Ruta de la imagen.
     * @return True si pudo cargar la imagen.
     */
    public boolean setTarget(String path){
        //TargetRGB = Imgcodecs.imread(path);
        if(path == null) return false;
        if(TargetRGB == null)
            TargetRGB = new Mat();
        Imgproc.cvtColor(Imgcodecs.imread(path), TargetRGB,Imgproc.COLOR_BGR2RGB);
        updateTarget();

        return true;
    }


    public  boolean setSourse(byte []image,int w, int h)
    {
        if(SourceRGB == null)
            SourceRGB = new Mat();
        OpenCVNativeClass.byteToMat(SourceRGB.nativeObj,image,w,h);
        return true;
    }

    /***
     * Convierte el target al espacio de color correspondiente y obtiene sus estadísticas.
     * @return True si pudo realizar el proceso.
     */
    public boolean updateTarget()
    {
        if(TargetRGB == null) return false;
        if (!isRGB)
            if(!isRLAB)
                Imgproc.cvtColor(TargetRGB, Target, convCode);
            else {
                if(auxT == null)
                    auxT = new Mat();
                TargetRGB.convertTo(auxT,CvType.CV_32FC3);
                OpenCVNativeClass.RGB2RLab(auxT.nativeObj, Target.nativeObj);
            }
        else
            TargetRGB.copyTo(Target);

        Core.meanStdDev(Target, mUt, mDt);
        Ut = new Scalar(mUt.toArray());
        Dt = new Scalar(mDt.toArray());

        return true;
    }

    /***
     * Convierte el source al espacio de color correspondiente y obtiene sus estadísticas.
     * @return True si pudo realizar el proceso.
     */
    public boolean updateSourse()
    {
        if(SourceRGB == null)
            return false;
        if(SourceRGB.empty())return false;
        if (!isRGB) {
            if (!isRLAB)
                Imgproc.cvtColor(SourceRGB, Source, convCode);
            else {
                /*if(auxS == null)
                    auxS = new Mat();*/
                SourceRGB.convertTo(auxS,CvType.CV_32FC3);
                OpenCVNativeClass.RGB2RLab(auxS.nativeObj, Source.nativeObj);
            }
        }
        else
            SourceRGB.copyTo(Source);

        Core.meanStdDev(Source, mUs, mDs);
        Us = new Scalar(mUs.toArray());
        Ds = new Scalar(mDs.toArray());
        return true;
    }

    /***
     * Aplica el color transfer entre el source y el target y lo almacena en ResultRGB como una imagen RGB.
     */
    public void applyTransfer()
    {
        if(SourceRGB == null || TargetRGB == null)
            return;

        //Source.copyTo(Result);

        if(isRLAB)
            OpenCVNativeClass.ColorTransfer(Source.nativeObj,Result.nativeObj,mUs.toArray(),mDs.toArray(),mUt.toArray(),mDt.toArray());
        else {
            Scalar DtDs = new Scalar(Dt.val[0] / Ds.val[0], Dt.val[1] / Ds.val[1], Dt.val[2] / Ds.val[2]);
            Core.subtract(Source, Us, Result, new Mat(), CvType.CV_32F);
            Core.multiply(Result, DtDs, auxS, 1, CvType.CV_32F);
            Core.add(auxS, Ut, Result, new Mat(), CvType.CV_8UC4);
        }

        if (!isRGB)
        {
            if(!isRLAB)
                Imgproc.cvtColor(Result, ResultRGB, invConvCode);
            else
            {
                OpenCVNativeClass.RLab2RGB(Result.nativeObj,ResultRGB.nativeObj);
                ResultRGB.convertTo(ResultRGB,CvType.CV_8UC4);
            }
        }
        else
            Result.copyTo(ResultRGB);
    }

    /***
     * Asigna el color space en el que se hará el color transfer.
     * @param code Código del espacio en que se realiza el proceso.
     */
    void setColorSpace(Code code)
    {
        isRGB = false;
        isRLAB = false;

        if (code == Code.LAB)
        {
            convCode = Imgproc.COLOR_LRGB2Lab;
            invConvCode = Imgproc.COLOR_Lab2LRGB;
        }
        else if (code == Code.LUV)
        {
            convCode = Imgproc.COLOR_LRGB2Luv;
            invConvCode = Imgproc.COLOR_Luv2LRGB;
        }
        else if (code == Code.YCRCB)
        {
            convCode = Imgproc.COLOR_RGB2YCrCb;
            invConvCode = Imgproc.COLOR_YCrCb2RGB;
        }
        else if (code == Code.HSV)
        {
            convCode = Imgproc.COLOR_RGB2HSV;
            invConvCode = Imgproc.COLOR_HSV2RGB;
        }
        else if(code  == Code.RGB) {
            convCode = -1;
            isRGB = true;
        }
        else {
            convCode = -1;
            isRLAB = true;
        }
    }

    void setFullAlpha()
    {
        Scalar a = new Scalar(0, 0, 0, 255);
        Core.add(ResultRGB, a, ResultRGB);
    }

}
