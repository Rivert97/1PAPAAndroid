package com.imagenes.procesamiento.roberto.opencvtry2;

import android.media.Image;
import android.view.Surface;

import org.opencv.core.Mat;

import java.nio.ByteBuffer;

public class OpenCVNativeClass {
    //Función de prueba de OpenCV
    public native static int convertGray(long matAddrRgba, long matAddrGray);
    //Aplicar transfer (la usamos cuando es RLAB)
    public native static int ColorTransfer(long matRgb, long dst, double us [],double ds[],double ut[],double dt[]);
    //Procesar imagen con la API de camera2 (prueba)
    public native static int ProcessWCamera2(int srcWidth, int srcHeight, ByteBuffer srcBuffer, Surface dst);
    //Procesar Mat con API de camera2 (prueba)
    public native static int ProcessMat(int srcWidth, int srcHeight, long matYuv, Surface dst);
    //Converite de byte a Mat
    public native static int byteToMat(long mat, byte [] us,int w,int h);
    //Conversión de RGB a RLAB
    public native static int RGB2RLab(long matAddrRgba, long matAddrGray);
    //Conversión de RLAB a RGB
    public native static int RLab2RGB(long matAddrRgba, long matAddrGray);
    //Obtener mat de Image
    public native static int GetMat(int srcWidth, int srcHeight, ByteBuffer srcBuffer, long dst);
    //Mostrar la Mat en la superficie indicada
    public native static int ShowMat(int srcWidth, int srcHeight, long src, Surface dst);
}
