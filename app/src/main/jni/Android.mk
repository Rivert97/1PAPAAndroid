LOCAL_PATH := $(call my-dir)

	include $(CLEAR_VARS)

	#opencv
	OPENCVROOT:= /home/dezcorjm/OpenCV-android-sdk
	OPENCV_CAMERA_MODULES:=on
	OPENCV_INSTALL_MODULES:=on
	OPENCV_LIB_TYPE:=SHARED
	include ${OPENCVROOT}/sdk/native/jni/OpenCV.mk

	LOCAL_SRC_FILES := com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass.cpp

	LOCAL_LDLIBS += -llog -landroid
	LOCAL_MODULE := MyOpencvLibs


	include $(BUILD_SHARED_LIBRARY)