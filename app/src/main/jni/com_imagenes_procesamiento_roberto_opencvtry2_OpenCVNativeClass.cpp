#include <com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass.h>


JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_convertGray
  (JNIEnv *, jclass, jlong addrRgba, jlong addrGray){
    Mat& mRgb = *(Mat*)addrRgba;
    Mat& mGray = *(Mat*)addrGray;

    int conv;
    jint retVal;
    //conv = toGray(mRgb, mGray);
    conv = Negative(mRgb, mGray);

    retVal = (jint) conv;

    return retVal;
  }

  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_RGB2RLab
    (JNIEnv *, jclass, jlong S, jlong R)
    {
        Mat& mS = *(Mat*)S;
        Mat& mR = *(Mat*)R;
        CvtColortoRLAB(mS,mR);
        return 0;
    }

  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_RLab2RGB
    (JNIEnv *, jclass, jlong S, jlong R)
    {
        Mat& mS = *(Mat*)S;
        Mat& mR = *(Mat*)R;
        CvtColortoBGR(mS,mR);
        return 0;
    }


  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_ColorTransfer
    (JNIEnv * env, jclass, jlong S,jlong dts, jdoubleArray us, jdoubleArray ds, jdoubleArray ut, jdoubleArray dt)
    {
        Mat& sourse = *(Mat*)S;
        Mat& Result = *(Mat*)dts;
        cv::Mat auxS;

        jdouble * mUs = env->GetDoubleArrayElements(us,0);
        jdouble * mDs = env->GetDoubleArrayElements(ds,0);
        jdouble * mUt = env->GetDoubleArrayElements(ut,0);
        jdouble * mDt = env->GetDoubleArrayElements(dt,0);

        cv::Scalar Ut(mUt[0],mUt[1],mUt[2]);
        cv::Scalar Us(mUs[0],mUs[1],mUs[2]);
        cv::Scalar Dt(mDt[0],mDt[1],mDt[2]);
        cv::Scalar Ds(mDs[0],mDs[1],mDs[2]);

        cv::Scalar mDtDs(mDt[0] / mDs[0], mDt[1] / mDs[1], mDt[2] / mDs[2]);

        cv::subtract(sourse,Us,Result,cv::noArray(),CV_32F);
        cv::multiply(Result,mDtDs,auxS,1,CV_32F);
        cv::add(auxS,Ut,Result,cv::noArray(),CV_32F);

        return 0;
    }

  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_byteToMat
    (JNIEnv *env, jobject obj, jlong S, jbyteArray byte, jint w,jint h)
    {
        Mat& sourse = *(Mat*)S;
        sourse.reshape(w,h);
        jbyte * data = env ->GetByteArrayElements(byte,0);
        cv::Mat mYuv(w,h, CV_8UC4, data);
        mYuv.copyTo(sourse);
        return 0;
    }

  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_GetMat
    (JNIEnv *env, jobject obj, jint srcWidth, jint srcHeight, jobject srcBuffer, jlong dst)
    {
        uint8_t *srcLumaPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(srcBuffer));
        Mat& mat = *(Mat*)dst;

        if (srcLumaPtr == nullptr) {
            return 1;
        }

        int dstWidth;
        int dstHeight;

        dstWidth = srcHeight;
        dstHeight = srcWidth;

        Mat mYuv(srcHeight + srcHeight / 2, srcWidth, CV_8UC1, srcLumaPtr);

        Mat srcRgba(srcHeight, srcWidth, CV_8UC4);
        Mat flipRgba(dstHeight, dstWidth, CV_8UC4);

        // convert YUV -> RGBA
        //cv::cvtColor(mYuv, srcRgba, CV_YUV2BGRA_I420);
        cv::cvtColor(mYuv, srcRgba, CV_YUV2RGBA_NV21);

        // Rotate 90 degree
        cv::transpose(srcRgba, flipRgba);
        cv::flip(flipRgba, mat, 1);

        return 0;
    }

  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_ShowMat
    (JNIEnv *env, jobject obj, jint srcWidth, jint srcHeight, jlong src, jobject dstSurface)
    {
        Mat& srcRgb = *(Mat*)src;
        Mat Rgba;
        cvtColor(srcRgb, Rgba, CV_RGB2RGBA);

        uint8_t *srcChromaUVInterleavedPtr = nullptr;
        bool swapDstUV;

        ANativeWindow *win = ANativeWindow_fromSurface(env, dstSurface);
        ANativeWindow_acquire(win);

        ANativeWindow_Buffer buf;

        int dstWidth = srcWidth;//////
        int dstHeight = srcHeight;//////

        ANativeWindow_setBuffersGeometry(win, dstWidth, dstHeight, 0);

        if (int32_t err = ANativeWindow_lock(win, &buf, NULL)) {
            ANativeWindow_release(win);
            return 1;
        }

        uint8_t *dstLumaPtr = reinterpret_cast<uint8_t *>(buf.bits);
        Mat dstRgba(dstHeight, buf.stride, CV_8UC4, dstLumaPtr);        // TextureView buffer, use stride as width

        // copy to TextureView surface
        uchar *dbuf;
        uchar *sbuf;
        dbuf = dstRgba.data;
        sbuf = Rgba.data;
        int i;
        memcpy(dbuf, sbuf, Rgba.cols * Rgba.rows * 4);

        ANativeWindow_unlockAndPost(win);
        ANativeWindow_release(win);

        return 0;
    }

  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_ProcessWCamera2
    (JNIEnv *env, jobject obj, jint srcWidth, jint srcHeight, jobject srcBuffer, jobject dstSurface)
    {
        uint8_t *srcLumaPtr = reinterpret_cast<uint8_t *>(env->GetDirectBufferAddress(srcBuffer));

        if (srcLumaPtr == nullptr) {
            return 1;
        }

        int dstWidth;
        int dstHeight;

        cv::Mat mYuv(srcHeight + srcHeight / 2, srcWidth, CV_8UC1, srcLumaPtr);

        uint8_t *srcChromaUVInterleavedPtr = nullptr;
        bool swapDstUV;

        ANativeWindow *win = ANativeWindow_fromSurface(env, dstSurface);
        ANativeWindow_acquire(win);

        ANativeWindow_Buffer buf;

        dstWidth = srcHeight;
        dstHeight = srcWidth;
    //    dstWidth = srcWidth;
    //    dstHeight = srcHeight;

        ANativeWindow_setBuffersGeometry(win, dstWidth, dstHeight, 0);

        if (int32_t err = ANativeWindow_lock(win, &buf, NULL)) {
            //LOGE("ANativeWindow_lock failed with error code %d\n", err);
            ANativeWindow_release(win);
            return 1;
        }

        uint8_t *dstLumaPtr = reinterpret_cast<uint8_t *>(buf.bits);
        Mat dstRgba(dstHeight, buf.stride, CV_8UC4, dstLumaPtr);        // TextureView buffer, use stride as width
        Mat srcRgba(srcHeight, srcWidth, CV_8UC4);
        Mat flipRgba(dstHeight, dstWidth, CV_8UC4);

        // convert YUV -> RGBA
        //cv::cvtColor(mYuv, srcRgba, CV_YUV2BGRA_I420);
        cv::cvtColor(mYuv, srcRgba, CV_YUV2RGBA_NV21);

        // Rotate 90 degree
        cv::transpose(srcRgba, flipRgba);
        cv::flip(flipRgba, flipRgba, 1);
        //cv::cvtColor(flipRgba, flipRgba, CV_RGB2GRAY);

        // copy to TextureView surface
        uchar *dbuf;
        uchar *sbuf;
        dbuf = dstRgba.data;
        sbuf = flipRgba.data;
        int i;
        /*for(i=0;i<flipRgba.rows;i++) {
            dbuf = dstRgba.data + i * buf.stride * 4;
            memcpy(dbuf, sbuf, flipRgba.cols * 4);
            sbuf += flipRgba.cols * 4;
        }*/
        memcpy(dbuf,sbuf,flipRgba.cols * flipRgba.rows * 4);

        // Draw some rectangles
        /*Point p1(100, 100);
        Point p2(300, 300);
        cv::rectangle(dstRgba, p1, p2, Scalar(255, 255, 255));
        cv::rectangle(dstRgba, Point(10, 10), Point(dstWidth - 1, dstHeight - 1),
                      Scalar(255, 255, 255));
        cv::rectangle(dstRgba, Point(100, 100), Point(dstWidth / 2, dstWidth / 2),
                      Scalar(255, 255, 255));*/

        //LOGE("bob dstWidth=%d height=%d", dstWidth, dstHeight);
        ANativeWindow_unlockAndPost(win);
        ANativeWindow_release(win);

        return 0;
    }

  JNIEXPORT jint JNICALL Java_com_imagenes_procesamiento_roberto_opencvtry2_OpenCVNativeClass_ProcessMat
    (JNIEnv *env, jobject obj, jint srcWidth, jint srcHeight, jlong mat, jobject dstSurface)
    {
        // Check that if src chroma channels are interleaved if element stride is 2.
        // Our Halide kernel "directly deinterleaves" UVUVUVUV --> UUUU, VVVV
        // to handle VUVUVUVU, just swap the destination pointers.

        Mat mYuv = *((Mat*)mat);

        //cv::Mat mYuv(srcHeight,srcWidth, CV_8UC3, srcLumaPtr);

        ANativeWindow *win = ANativeWindow_fromSurface(env, dstSurface);
        ANativeWindow_acquire(win);

        ANativeWindow_Buffer buf;

        if (int32_t err = ANativeWindow_lock(win, &buf, NULL)) {
            ANativeWindow_release(win);
            return 1;
        }

        ANativeWindow_setBuffersGeometry(win, srcWidth, srcHeight, 0 /*format unchanged*/);
        uint8_t *dstLumaPtr = reinterpret_cast<uint8_t *>(buf.bits);
        cv::Mat mRgba(srcHeight, srcWidth, CV_8UC4, dstLumaPtr);
        cv::Mat flipRgba(srcHeight, srcWidth, CV_8UC4);

        cv::cvtColor(mYuv, mRgba, CV_YUV2RGBA_NV21);
        //mRgba = mYuv;
        //cv::transpose(mYuv, mRgba);
        // Rotate 90 degree
        cv::transpose(mRgba, flipRgba);
        cv::flip(flipRgba, flipRgba, 1);

        ANativeWindow_unlockAndPost(win);
        ANativeWindow_release(win);
        return 0;

    }



  int toGray(const Mat &img, Mat& gray)
  {
    cvtColor(img, gray, CV_RGBA2GRAY);
    if(gray.rows == img.rows && gray.cols == img.cols)
        return 1;

    return 0;
  }

  int Negative(const Mat &img, Mat& neg)
  {
    Vec4b pixel;
    for (int j = 0; j < img.rows; j++) {
        for (int i = 0; i < img.cols; i++) {
            pixel = img.at<Vec4b>(j, i);
            for (int k = 0; k < 4; k++) {
                int a = 255 - (int)pixel[k];
                neg.at<Vec4b>(j, i)[k] = (uchar)a;
            }
        }
    }
    return 1;
  }

  void CvtColortoRLAB(const cv::Mat &m,cv::Mat& dst)
  {
      cv::Mat src;
      cv::cvtColor(m,dst,cv::COLOR_RGB2XYZ);
      const int W = dst.cols;
      const int H = dst.rows;
      const float Lsize = W * H;
      const float f=1/3.5;
      cv::Vec3f vec(0,0,0);
      float l,a,b;
      bool s=false;
      for (int i = 0; i < H; i++)
      {
          for (int j = 0; j < W; j++)
          {
              vec=dst.at<cv::Vec3f>(i, j);
              xyz2xryrzr(vec[0],vec[1],vec[2]);
              l = 100 *cv::pow(vec[1],f);
              a = 430 *(cv::pow(vec[0],f)-cv::pow(vec[1],f));
              b = 170 *(cv::pow(vec[1],f)-cv::pow(vec[2],f));
              dst.at<cv::Vec3f>(i, j) = cv::Vec3f(l,a,b);
          }
      }
  }

  void CvtColortoBGR(const cv::Mat &m,cv::Mat& dst)
  {
    cv::Mat aux;
    m.copyTo(aux);
    const int W = aux.cols;
    const int H = aux.rows;
    const float f=1/3.5;
    const float fi = 3.5;
    cv::Vec3f vec(0,0,0);
    float y,x,z;
    for (int i = 0; i < H; i++)
    {
        for (int j = 0; j < W; j++)
        {
            vec=aux.at<cv::Vec3f>(i, j);
            y = cv::pow(vec[0]/100.0,fi);
            if( std::isnan(y))
                y = 0;
            x = cv::pow(vec[1]/430.0+cv::pow(y,f),fi);
            if( std::isnan(x))
                x = 0;
            z = cv::pow(cv::pow(y,f)-vec[2]/170.0,fi);
            if( std::isnan(z))
                z = 0;
            xryrzr2xyz(x,y,z);
            aux.at<cv::Vec3f>(i, j) = cv::Vec3f(x,y,z);
        }
    }
    cv::cvtColor(aux,dst,cv::COLOR_XYZ2RGB);
  }

  void xyz2xryrzr(float &x,float &y,float &z)
  {
    float a[3][3] ={
       1.002,-0.0401,0.0084,
       -0.0042,0.9666,0.0008,
       0,0,0.9110
    };
    float xu,yu,zu;
    xu = x * a[0][0] + y * a[0][1] + z * a[0][2];
    yu = x * a[1][0] + y * a[1][1] + z * a[1][2];
    zu = x * a[2][0] + y * a[2][1] + z * a[2][2];
    x= xu;
    y = yu;
    z = zu;
  }

  void xryrzr2xyz(float &x,float &y,float &z)
  {
    float a[3][3] ={
           9.98177567e-01,   4.14100149e-02, -9.24019712e-03,
           4.33720855e-03,   1.03473404e+00,  -9.48649597e-04,
           0.00000000e+00,   0.00000000e+00,   1.09769484e+00
        };
    float xu,yu,zu;
    xu = x * a[0][0] + y * a[0][1] + z * a[0][2];
    yu = x * a[1][0] + y * a[1][1] + z * a[1][2];
    zu = x * a[2][0] + y * a[2][1] + z * a[2][2];
    x= xu;
    y = yu;
    z = zu;
  }
